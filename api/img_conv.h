#pragma once
#include <stdint.h>

#define   IC_ERROR        -1
#define   IC_ERROR_NONE   0

typedef enum 
{
	IC_MATRIX_RGB                      = 0,
	IC_MATRIX_BT709                    = 1,
	IC_MATRIX_UNSPECIFIED              = 2,
	IC_MATRIX_FCC                      = 4,
	IC_MATRIX_BT470_BG                 = 5,
	IC_MATRIX_ST170_M                  = 6,  /* Equivalent to 5. */
	IC_MATRIX_ST240_M                  = 7,
	IC_MATRIX_YCGCO                    = 8,
	IC_MATRIX_BT2020_NCL               = 9,
	IC_MATRIX_BT2020_CL                = 10,
	IC_MATRIX_CHROMATICITY_DERIVED_NCL = 12, /* Requires primaries to be set. */
	IC_MATRIX_CHROMATICITY_DERIVED_CL  = 13, /* Requires primaries to be set. */
	IC_MATRIX_ICTCP                    = 14
} ic_matrix_coefficients;

typedef enum
{
	IC_PRIMARIES_BT709       = 1,
	IC_PRIMARIES_UNSPECIFIED = 2,
	IC_PRIMARIES_BT470_M     = 4,
	IC_PRIMARIES_BT470_BG    = 5,
	IC_PRIMARIES_ST170_M     = 6,
	IC_PRIMARIES_ST240_M     = 7,  /* Equivalent to 6. */
	IC_PRIMARIES_FILM        = 8,
	IC_PRIMARIES_BT2020      = 9,
	IC_PRIMARIES_ST428       = 10,
	IC_PRIMARIES_ST431_2     = 11,
	IC_PRIMARIES_ST432_1     = 12,
	IC_PRIMARIES_EBU3213_E   = 22
} ic_color_primaries;

typedef enum 
{
	IC_TRANSFER_BT709         = 1,
	IC_TRANSFER_UNSPECIFIED   = 2,
	IC_TRANSFER_BT470_M       = 4,
	IC_TRANSFER_BT470_BG      = 5,
	IC_TRANSFER_BT601         = 6,  /* Equivalent to 1. */
	IC_TRANSFER_ST240_M       = 7,
	IC_TRANSFER_LINEAR        = 8,
	IC_TRANSFER_LOG_100       = 9,
	IC_TRANSFER_LOG_316       = 10,
	IC_TRANSFER_IEC_61966_2_4 = 11,
	IC_TRANSFER_IEC_61966_2_1 = 13,
	IC_TRANSFER_BT2020_10     = 14, /* Equivalent to 1. */
	IC_TRANSFER_BT2020_12     = 15, /* Equivalent to 1. */
	IC_TRANSFER_ST2084        = 16,
	IC_TRANSFER_ARIB_B67      = 18
} ic_transfer_characteristics;

typedef enum 
{
	IC_RANGE_LIMITED  = 0,  /**< Studio (TV) legal range, 16-235 in 8 bits. */
	IC_RANGE_FULL     = 1   /**< Full (PC) dynamic range, 0-255 in 8 bits. */
} ic_pixel_range;

typedef enum
{
	IC_YUV444 = 0,
	IC_YUV422 = 1,
	IC_YUV420 = 2,
} ic_chroma_format;

typedef enum
{
  IC_R0 = 0,
  IC_R90,
  IC_R180,
  IC_R270,
} ic_rotate_degree;

typedef enum
{
	IC_TM_LINEAR = 0,
	IC_TM_HABLE = 1,
  IC_TM_ITUR_BT2446_A,
  IC_TM_ITUR_BT2446_B,
  IC_TM_ITUR_BT2446_C,
} ic_tonemap;

typedef enum
{
	IC_ITM_LINEAR = 0,
  IC_ITM_ITUR_BT2408_DISPLAY_REFFERED,
  IC_ITM_ITUR_BT2446_A,
  IC_ITM_ITUR_BT2446_B,
  IC_ITM_ITUR_BT2446_C,
} ic_inv_tonemap;

typedef struct
{
  int width;
  int height;
  int pixel_depth;
	ic_chroma_format chroma_format;
  ic_pixel_range signal_range;
	ic_transfer_characteristics trc;
  ic_color_primaries prim;
  ic_matrix_coefficients mat_coeff; 
} ic_image_format;

typedef struct
{
	uint8_t* planes[3];
	uint32_t w[3];
	uint32_t h[3];
	uint32_t stride[3];
} ic_image_buffer;

typedef struct
{
  // common
  int thread_count;
  int chunk_size;
  ic_image_format input_format;
  ic_image_format output_format;
  ic_rotate_degree rotate;
  
  // HDR
  double peak_luminance;
  ic_tonemap tonemap;
  ic_inv_tonemap inv_tonemap;  
} ic_config;

typedef void* ic_instance;

typedef struct 
{
  void* user_context;
  void (*request)(void* user_context, ic_image_buffer* output);
  void (*on_receive)(void* user_context, const ic_image_buffer* buffer);
} ic_frame_receiver_params;


#if __cplusplus
extern "C" {
#endif

int img_conv_create(ic_instance* p_inst, const ic_config* config);

int img_conv_set_frame_receiver(ic_instance inst, ic_frame_receiver_params* param);

int img_conv_put_frame(ic_instance inst, const ic_image_buffer* input);

int img_conv_get_frame(ic_instance inst, ic_image_buffer* output, int input_terminated);

int img_conv_destroy(ic_instance inst);

#if __cplusplus
}
#endif