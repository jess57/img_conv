#pragma once
#include "common.h"

typedef struct 
{
  uint8_t src_offset;
  uint8_t dst_offset;
  uint8_t src_depth;
  uint8_t dst_depth;
  uint8_t target_plane;
  uint16_t clip_min;
  uint16_t clip_max;
} IcDepthConvParam;

IcFilterFunc get_depthconv_func_c(int input_depth, int output_depth);
IcFilterFunc get_depthconv_func_avx2(int input_depth, int output_depth);