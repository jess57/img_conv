#include "depth.h"

static int depth_conv_8bit_to_8bit_rs(IcFilterParam* param)
{
  IcDepthConvParam* conv_param = (IcDepthConvParam*)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, offset, val;

  shift = conv_param->src_depth - conv_param->dst_depth;
  offset = conv_param->src_offset + (1 << (shift - 1));
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((param->input.planes[c][i * param->input.stride[c] + j] + offset) >> shift) + conv_param->dst_offset;
      param->output.planes[c][i * param->output.stride[c] + j] = (uint8_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

static int depth_conv_8bit_to_8bit_ls(IcFilterParam* param)
{
  IcDepthConvParam *conv_param = (IcDepthConvParam *)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, val;

  shift = conv_param->dst_depth - conv_param->src_depth;
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((param->input.planes[c][i * param->input.stride[c] + j] + conv_param->src_offset) << shift) + conv_param->dst_offset;
      param->output.planes[c][i * param->output.stride[c] + j] = (uint8_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

static int depth_conv_8bit_to_16bit(IcFilterParam* param)
{
  IcDepthConvParam* conv_param = (IcDepthConvParam*)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, val;
  uint16_t* dst = (uint16_t*)param->output.planes[c];

  shift = conv_param->dst_depth - conv_param->src_depth;
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((param->input.planes[c][i * param->input.stride[c] + j] + conv_param->src_offset) << shift) + conv_param->dst_offset;
      dst[i * param->output.stride[c] / 2 + j] = (uint16_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

static int depth_conv_16bit_to_8bit(IcFilterParam* param)
{
  IcDepthConvParam* conv_param = (IcDepthConvParam*)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, offset, val;
  const uint16_t *src = (const uint16_t*)param->input.planes[c];

  shift = conv_param->src_depth - conv_param->dst_depth;
  offset = conv_param->src_offset + (1 << (shift - 1));
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((src[i * param->input.stride[c] / 2 + j] + offset) >> shift) + conv_param->dst_offset;
      param->output.planes[c][i * param->output.stride[c] + j] = (uint8_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

static int depth_conv_16bit_to_16bit_rs(IcFilterParam* param)
{
  IcDepthConvParam* conv_param = (IcDepthConvParam*)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, offset, val;
  const uint16_t *src = (const uint16_t*)param->input.planes[c];
  uint16_t* dst = (uint16_t*)param->output.planes[c];

  shift = conv_param->src_depth - conv_param->dst_depth;
  offset = conv_param->src_offset + (1 << (shift - 1));
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((src[i * param->input.stride[c] / 2 + j] + offset) >> shift) + conv_param->dst_offset;
      dst[i * param->output.stride[c] / 2 + j] = (uint16_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

static int depth_conv_16bit_to_16bit_ls(IcFilterParam* param)
{
  IcDepthConvParam *conv_param = (IcDepthConvParam *)param->priv;
  int i, j;
  int c = conv_param->target_plane;
  int shift, val;
  const uint16_t *src = (const uint16_t *)param->input.planes[c];
  uint16_t *dst = (uint16_t *)param->output.planes[c];

  shift = conv_param->dst_depth - conv_param->src_depth;
  for (i = 0; i < param->input.h[c]; i++) {
    for (j = 0; j < param->input.w[c]; j++) {
      val = ((src[i * param->input.stride[c] / 2 + j] + conv_param->src_offset) << shift) + conv_param->dst_offset;
      dst[i * param->output.stride[c] / 2 + j] = (uint16_t)IC_CLIP3(val, conv_param->clip_min, conv_param->clip_max);
    }
  }

  return IC_ERROR_NONE;
}

IcFilterFunc get_depthconv_func_c(int input_depth, int output_depth)
{
  if(input_depth <= 8 && output_depth <= 8)
  {
    if(input_depth > output_depth)
      return depth_conv_8bit_to_8bit_rs;
    else
      return depth_conv_8bit_to_8bit_ls;
  }
  else if(input_depth <= 8)
    return depth_conv_8bit_to_16bit;
  else if(output_depth <= 8)
    return depth_conv_16bit_to_8bit;
  else
  {
    if(input_depth > output_depth)
      return depth_conv_16bit_to_16bit_rs;
    else
      return depth_conv_16bit_to_16bit_ls;
  }
}