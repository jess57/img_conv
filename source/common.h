#pragma once
#include "img_conv.h"

#define IC_MIN(x, y)                  ((x) < (y)) ? (x) : (y)
#define IC_MAX(x, y)                  ((x) > (y)) ? (x) : (y)
#define IC_CLIP3(x, min_val, max_val) IC_MIN(IC_MAX(x, min_val), max_val)

typedef struct
{
  ic_image_buffer input;
  ic_image_buffer output;
  void            *priv;
} IcFilterParam;

typedef int (*IcFilterFunc)(IcFilterParam* param);


